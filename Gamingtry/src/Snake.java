
import javax.swing.JFrame;//pentru a crea un frame API

public class Snake extends JFrame { //extinde JFrame pentru a defini o fereastra

	/**
	 * 
	 */
	private static final long serialVersionUID = -1503268438955605275L;

	public Snake() {		//contructor implicit
		add(new Board()); //adaugarea clasei Board
		setResizable(false);//nu se redimensioneaza
		pack();//aranjeaza
		setTitle("Snake");//seteaza titlu
		setLocationRelativeTo(null);//fixeaza frame-ul la centru
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);//evenimentul pentru eliminarea la inchidere
		setVisible(true);//seteaza vizibil
		setSize(600, 600);//seteaza dimensiunea
	}

	public static void main(String[] args) {
		new Snake();//initializeaza clasa
	}
}