# Project Title

First game developed in JAVA Swing

It is using files of I/O and threads for the movement.

## Getting Started

Run snake.jar

![Screenshot_5](/uploads/8f59fa800185ebf8d03fc90428046a59/Screenshot_5.jpg)

![Screenshot_3](/uploads/d8fea4cc2665b41e229b9066222743be/Screenshot_3.jpg) 
![Screenshot_4](/uploads/1d9030c11ab5f51bc3406b0976cea00e/Screenshot_4.jpg)


![Screenshot_2](/uploads/a921a942e10e2c8ab0e031a3bc1ba77a/Screenshot_2.jpg)
### Prerequisites

Download the file snake.jar you need to install java  ``` https://java.com/en/download/ ``` or run the entire project using snake.java


### Installing

Run snake.jar

or using git

```
 git clone https://gitlab.com/cristian.cernat97Java/SnakeThreads.git
```

1. Launch the file;
2. Search on browser localhost:8080;
3. Enjoy, choose what search do you want to use DFS or BFS.
```
## Built With

* [Java](http://www.oracle.com/technetwork/java/javase/downloads/jdk10-downloads-4416644.html) - JDK
* [SWING, AWT] - MVC


## Contributing

Go ahead blame me for anything I am still learning 
## Versioning

Version 1.0 

## Authors

* **Cristian Cernat** - (https://gitlab.com/cristian.cernat97)